import React, { useState } from "react";
import { AiOutlineMail, AiOutlinePhone, AiOutlineEdit } from "react-icons/ai";
import { IoEarth } from "react-icons/io5";
import "../../assets/css/model.css";
import classes from "../../assets/css/cardDetails.module.css";
import { BsHeart, BsFillHeartFill } from "react-icons/bs";
import { RiDeleteBin7Fill } from "react-icons/ri";
import { useDispatch } from "react-redux";
import { likeChanged, deleteItem, editData } from "../../store/userStore";
import { Modal } from "react-bootstrap";
import { validEmail } from "../../utils/checkEmail";
import { checkIsNotEmpty } from "../../utils/checkIsNotEmpty";
import { HTTP_TEXT } from "../../assets/constants/constants";

const CardDetails = (props) => {
  const [isShow, setIsShow] = useState(false);

  // input states
  const [inputValue, setInputValue] = useState({
    name: "",
    email: "",
    phone: "",
    website: "",
  });

  // is valid states
  const [isValid, setIsValid] = useState({
    isNameValid: true,
    isEmailValid: true,
    isPhoneValid: true,
    isWebsiteValid: true,
  });

  // is input chnage or not
  const [isInputChange, setIsInputChange] = useState({
    isNameInputChange: false,
    isEmailInputChange: false,
    isPhoneInputChange: false,
    isWebsiteInputChange: false,
  });

  // error states
  const [error, setError] = useState({
    nameError: "",
    emailError: "",
    phoneError: "",
    websiteError: "",
  });

  const dispatch = useDispatch();

  // click on like button
  const onClickOnLikeButton = () => {
    dispatch(likeChanged(props.data.id));
  };

  // click on bin button
  const onClickOnBin = () => {
    dispatch(deleteItem(props.data.id));
  };

  // model hide or show
  const doModelHide = () => {
    setIsShow(!isShow);
  };

  // when submit edited data
  const onChangeOfData = (event) => {
    event.preventDefault();



    if (
      isValid.isEmailValid &&
      isValid.isNameValid &&
      isValid.isPhoneValid &&
      isValid.isWebsiteValid
    ) {

      let sendName = props.data.name;
      let sendEmail = props.data.email;
      let sendPhone = props.data.phone;
      let sendWebsite = props.data.website;

      if(isInputChange.isNameInputChange){
        sendName = inputValue.name
      }
      if(isInputChange.isEmailInputChange){
        sendEmail = inputValue.email
      }
      if(isInputChange.isPhoneInputChange){
        sendPhone = inputValue.phone
      }
      if(isInputChange.isWebsiteInputChange){
        sendWebsite = inputValue.website
      }
      let updatedUserData = {
        ...props.data,
        name: sendName,
        email: sendEmail,
        phone: sendPhone,
        website: sendWebsite,
      };

      dispatch(editData(updatedUserData));
      doModelHide();
    }
  };

  // on change of input
  const onInputOfName = (event) => {
    setIsInputChange((prevState) => {
      return { ...prevState, isNameInputChange: true };
    });

    let data = checkIsNotEmpty(event.target.value);

    setIsValid((prevState) => {
      return { ...prevState, isNameValid: data.isValid };
    });

    setInputValue((prevState) => {
      return { ...prevState, name: event.target.value };
    });

    setError((prevState) => {
      return { ...prevState, nameError: data.error };
    });
  };

  // on change of email
  const onInputOfEmail = (event) => {
    setIsInputChange((prevState) => {
      return { ...prevState, isEmailInputChange: true };
    });

    let data = validEmail(event.target.value);

    setIsValid((prevState) => {
      return { ...prevState, isEmailValid: data.isValid };
    });

    setInputValue((prevState) => {
      return { ...prevState, email: event.target.value };
    });

    setError((prevState) => {
      return { ...prevState, emailError: data.error };
    });
  };

  // on change of mobile number
  const onInputOfPhone = (event) => {
    setIsInputChange((prevState) => {
      return { ...prevState, isPhoneInputChange: true };
    });

    let data = checkIsNotEmpty(event.target.value);

    setIsValid((prevState) => {
      return { ...prevState, isPhoneValid: data.isValid };
    });

    setInputValue((prevState) => {
      return { ...prevState, phone: event.target.value };
    });

    setError((prevState) => {
      return { ...prevState, phoneError: data.error };
    });
  };

  // on change of website
  const onInputOfWebsite = (event) => {
    setIsInputChange((prevState) => {
      return { ...prevState, isWebsiteInputChange: true };
    });

    let data = checkIsNotEmpty(event.target.value);

    setIsValid((prevState) => {
      return { ...prevState, isWebsiteValid: data.isValid };
    });

    setInputValue((prevState) => {
      return { ...prevState, website: event.target.value };
    });

    setError((prevState) => {
      return { ...prevState, websiteError: data.error };
    });
  };

  return (
    <>
      <div className="container-fluid py-2">
        <div className="container-fluid px-4">
          <h5>{props.data.name}</h5>
          <p className="text-muted">
            <span className="fs-5">
              <AiOutlineMail />
            </span>
            &nbsp;&nbsp;{props.data.email}
          </p>
          <p className="text-muted">
            <span className="fs-5">
              <AiOutlinePhone />
            </span>
            &nbsp;&nbsp;{props.data.phone}
          </p>
          <p className="text-muted">
            <span className="fs-5">
              <IoEarth />
            </span>
            &nbsp;&nbsp;{HTTP_TEXT}
            {props.data.website}
          </p>
        </div>
      </div>
      <div className="container-fluid d-flex justify-content-between py-2 border">
        <div
          onClick={onClickOnLikeButton}
          className={`${classes["text-color"]} ${classes["cursor-pointer"]} fs-5`}
        >
          {props.data.isLiked ? <BsFillHeartFill /> : <BsHeart />}
        </div>
        <div
          className={`${classes["icon-color"]} ${classes["cursor-pointer"]} fs-5`}
        >
          <AiOutlineEdit onClick={doModelHide} />
        </div>
        <div
          onClick={onClickOnBin}
          className={`${classes["icon-color"]} ${classes["cursor-pointer"]} fs-5`}
        >
          <RiDeleteBin7Fill />
        </div>
        <Modal show={isShow} onHide={doModelHide}>
          <Modal.Header closeButton>
            <h3>Edit Profile</h3>
          </Modal.Header>
          <Modal.Body>
            <div>
              <form onSubmit={onChangeOfData}>
                <label>Name</label>
                <input
                  onChange={onInputOfName}
                  type="text"
                  defaultValue={props.data.name}
                  name="name"
                  placeholder="Your name.."
                />
                {!isValid.isNameValid && (
                  <p className={classes["error-text"]}>{error.nameError}</p>
                )}
                <label>Email</label>
                <input
                  onChange={onInputOfEmail}
                  type="text"
                  defaultValue={props.data.email}
                  name="email"
                  placeholder="Your email.."
                />
                {!isValid.isEmailValid && (
                  <p className={classes["error-text"]}>{error.emailError}</p>
                )}
                <label>Phone</label>
                <input
                  onChange={onInputOfPhone}
                  type="text"
                  defaultValue={props.data.phone}
                  name="phone"
                  placeholder="Your phone.."
                />
                {!isValid.isPhoneValid && (
                  <p className={classes["error-text"]}>{error.phoneError}</p>
                )}
                <label>Website</label>
                <input
                  onChange={onInputOfWebsite}
                  type="text"
                  defaultValue={props.data.website}
                  name="website"
                  placeholder="Your website.."
                />
                {!isValid.isWebsiteValid && (
                  <p className={classes["error-text"]}>{error.websiteError}</p>
                )}
                <div className="px-3 d-flex justify-content-between">
                  <button
                    onClick={doModelHide}
                    className="btn btn-primary"
                    type="button"
                  >
                    Cancel
                  </button>
                  <button className="btn btn-success" type="submit">
                    Save
                  </button>
                </div>
              </form>
            </div>
          </Modal.Body>
        </Modal>
      </div>
    </>
  );
};

export default CardDetails;
