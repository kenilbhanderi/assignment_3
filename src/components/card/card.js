import React from "react";
import classes from "../../assets/css/card.module.css";
import CardDetails from "./cardDetails";

const Card = (props) => {

  return (
    <div className={`container-fluid py-2 ${classes.card}`}>
      <div className={`container-fluid text-center ${classes['card-bg']}`}>
        <img
          className={classes.img}
          src={props.data.profileURL}
          alt="Avatar"
        />
      </div>
      <CardDetails data={props.data}/>
    </div>
  );
};

export default Card;
