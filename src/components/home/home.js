import React, { useEffect } from "react";
import Card from "../card/card";
import { useDispatch, useSelector } from "react-redux";
import { getData } from "../../store/userStore";

const Home = () => {
  const dispatch = useDispatch();
  const getUserData = useSelector((state) => state.user.userData);

  useEffect(() => {
    dispatch(getData());
  }, [dispatch]);

  const userData =
      getUserData.map((element) => {
        return (
          <div key={element.id} className="col-xl-3 col-lg-3 py-3 px-3">
            <Card data={element} />
          </div>
        );
      })


  return (
    <div className="container-fluid">
      <div className="row">{userData}</div>
    </div>
  );
};

export default Home;
