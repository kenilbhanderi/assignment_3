import "./assets/css/App.css";
import { Switch, Route, Redirect } from "react-router-dom";
import Home from "./components/home/home";

function App() {
  return (
    <Switch>
      <Route path="/users">
        <Home/>
      </Route>
      <Route path="*">
        <Redirect to='/users'/>
      </Route>
    </Switch>
  );
}

export default App;
