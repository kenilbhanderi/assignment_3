import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { USER_DATA_URL } from "../assets/constants/url";

export const getData = createAsyncThunk("user/getData", async () => {
  return await fetch(USER_DATA_URL).then((res) =>
    res.json()
  );
});

export const userSlice = createSlice({
  name: "user",
  initialState: {
    userData: [],
    status: null,
  },
  reducers: {
    likeChanged: (state, action) => {
      let tempArray = state.userData.filter((element) => {
        return element.id === action.payload;
      });
      tempArray[0].isLiked = !tempArray[0].isLiked;
    },
    deleteItem: (state, action) => {
      let tempArray = state.userData.filter((element) => {
        return element.id !== action.payload;
      });
      state.userData = tempArray;
    },
    editData: (state, action) => {
      const index = state.userData.findIndex((element)=> {
        return element.id === action.payload.id;
      })

      console.log(index)

      const newArray = [...state.userData];

      newArray[index].name = action.payload.name
      newArray[index].email = action.payload.email
      newArray[index].phone = action.payload.phone
      newArray[index].website = action.payload.website

      console.log(newArray);

      state.userData = newArray;
      
    },
  },
  extraReducers: {
    [getData.pending]: (state) => {
      state.status = "loading";
    },
    [getData.fulfilled]: (state, { payload }) => {
      let temArray = payload.map((element) => ({
        ...element,
        isLiked: false,
        profileURL: `https://avatars.dicebear.com/v2/avataaars/${element.username}.svg?options[mood][]=happy`,
      }));
      state.userData = temArray;
      state.status = "success";
    },
    [getData.rejected]: (state) => {
      state.status = "failed";
    },
  },
});

// Action creators are generated for each case reducer function
export const { likeChanged, deleteItem, editData } = userSlice.actions;

export default userSlice.reducer;
