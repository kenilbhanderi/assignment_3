import { EMAIL_INVALID_ERROR } from "../assets/constants/constants";

export const validEmail = (email) => {
    let isValid = false;
    let error = '';
    const emailRegex = /^\S+@\S+\.\S+$/;
    if(email.match(emailRegex)){
        isValid = true;
        error = '';
    }
    else{
        isValid = false;
        error = EMAIL_INVALID_ERROR;
    }

    return{
        isValid,
        error
    }
}