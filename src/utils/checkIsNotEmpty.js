import { EMPTY_FEILD_ERROR } from "../assets/constants/constants";

export const checkIsNotEmpty = (str) => {
  let isValid = false;
  let error = "";
  if (str.trim().length !== 0) {
    isValid = true;
    error = "";
  } else {
    isValid = false;
    error = EMPTY_FEILD_ERROR;
  }

  return {
    isValid,
    error,
  };
};
